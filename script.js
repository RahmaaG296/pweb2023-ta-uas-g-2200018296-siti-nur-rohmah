function displayGuestbookEntries() {
    var guestbookEntries = [
        { nama: 'John Doe', pesan: 'Selamat datang di website saya!' },
        { nama: 'Jane Smith', pesan: 'Terima kasih sudah berkunjung.' }
    ];

    var entriesList = document.getElementById('entries-list');
    entriesList.innerHTML = ''; // Hapus konten sebelumnya

    // Tambahkan setiap entri ke dalam daftar
    guestbookEntries.forEach(function (entry) {
        var listItem = document.createElement('li');
        listItem.innerHTML = '<strong>' + entry.nama + ':</strong> ' + entry.pesan;
        entriesList.appendChild(listItem);
    });
}

// Panggil fungsi displayGuestbookEntries() saat halaman dimuat
window.onload = function () {
    displayGuestbookEntries();
};